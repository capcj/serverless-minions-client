import React from "react";
import { Route, Switch } from "react-router-dom";
import AppliedRoute from "./components/core/AppliedRoute";
import Login from "./components/login/Login";
import Home from "./components/home/Home";
import NotFound from "./components/NotFound";

export default ({ childProps }) =>
<Switch>
  <AppliedRoute path="/" exact component={Login} props={childProps} />
  <AppliedRoute path="/login" exact component={Login} props={childProps} />
  <AppliedRoute path="/home" exact component={Home} props={childProps} />
  { /* Finally, catch all unmatched routes */ }
  <Route component={NotFound} />
</Switch>;
