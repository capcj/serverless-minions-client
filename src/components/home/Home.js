import * as React from 'react';
import { Row, Col } from 'react-bootstrap';
import "../../App.scss";

export default class Home extends React.Component {
    componentDidMount() {
        document.title += " - Home";
    }

    render() {
        return (
            <Row className="justify-content-md-center align-self-center">
                <Col md="4" className="minor-container">
                    <Row id="logo-container">
                        <Col>
                            <strong>Minion Store</strong>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Row>
                                <Col>
                                    HOME!
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        );
    }
}
