import * as React from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import { Auth } from "aws-amplify";
import './Login.scss';

export default class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: ""
        };
    }

    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();

        Auth.signIn(this.state.username, this.state.password)
            .then(() => {
                this.props.userHasAuthenticated(true);
                this.props.history.push("/home");
            })
            .catch(err => alert('Error signing in! ' + err.message));
    }

    componentDidMount() {
        document.title += " - Login";
    }

    render() {
        return (
            <Row className="justify-content-md-center align-self-center">
                <Col md="4" className="minor-container">
                    <Row id="logo-container">
                        <Col>
                            <strong>Minion Store</strong>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Row>
                                <Col>
                                    <Form onSubmit={this.handleSubmit}>
                                        <Form.Group controlId="username">
                                            <Form.Label>Username</Form.Label>
                                            <Form.Control
                                                autoFocus
                                                type="text"
                                                placeholder="Insert your username"
                                                value={this.state.username}
                                                onChange={this.handleChange}
                                            />
                                        </Form.Group>
                                        <Form.Group controlId="password">
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control
                                                type="password"
                                                placeholder="Insert your password"
                                                value={this.state.password}
                                                onChange={this.handleChange}
                                            />
                                        </Form.Group>
                                        <Button
                                            size="lg"
                                            block
                                            disabled={!this.validateForm()}
                                            type="submit"
                                        >
                                            Login
                                            </Button>
                                    </Form>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        );
    }
}
